package kz.avb.sample.simple.jetty.servlet;

import java.util.ResourceBundle;

public class AppException extends Exception {

    private static ResourceBundle resources;

    public AppException() {
        super();
        resources = ResourceBundle.getBundle(AppException.class.getName());
    }

    @Override
    public String getMessage() {
        return resources.getString(String.format("app.class.%s.message", this.getClass().getSimpleName()));
    }

    public static class InternalErrorException extends AppException {
    }

    public static class XmlWriteException extends AppException {
    }

    public static class UnsupportedHttpMethodException extends AppException {
    }

}
