package kz.avb.sample.simple.jetty.servlet;

import lombok.val;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Pattern;

import static java.util.Objects.isNull;

public class Servlet extends AbstractHandler {

    private static final Pattern pattern = Pattern.compile("^\\d{1,10}([\\.,]\\d{1,10})?$");

    @Override
    public void handle(String s, Request genericRequest,
                       HttpServletRequest request,
                       HttpServletResponse response)
            throws IOException {

        switch (request.getMethod()) {
            case "GET":
            case "POST":
                try {
                    writeAsXml(makeParamBean(request), response.getOutputStream());
                    applicationResponseStatus(response, null);
                } catch (AppException ex) {
                    applicationResponseStatus(response, ex);
                }
                break;
            default:
                applicationResponseStatus(response, new AppException.UnsupportedHttpMethodException());
        }
        genericRequest.setHandled(true);
    }

    private void writeAsXml(Parameters bean, ServletOutputStream out) throws AppException {

        if (isNull(bean) || isNull(out)) {
            throw new AppException.InternalErrorException();
        }

        try {
            val context = JAXBContext.newInstance(Parameters.class);
            val marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(bean, out);
        } catch (JAXBException e) {
            throw new AppException.XmlWriteException();
        }

    }

    private Parameters makeParamBean(HttpServletRequest request) {
        val bean = new Parameters();
        val decimalValues = new ArrayList<Parameters.Parameter>();
        val stringValues = new ArrayList<Parameters.Parameter>();

        for (Map.Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
            for (String value : entry.getValue()) {
                if (isNull(value) || value.trim().isEmpty()) {
                    stringValues.add(new Parameters.Parameter(entry.getKey(), null));
                    continue;
                }
                (pattern.matcher(value).find() ? decimalValues : stringValues)
                        .add(new Parameters.Parameter(entry.getKey(), value));
            }
        }

        bean.setNumerics(decimalValues.isEmpty() ? null : decimalValues);
        bean.setStrings(stringValues.isEmpty() ? null : stringValues);
        return bean;
    }

    private void applicationResponseStatus(HttpServletResponse response, Exception ex) {
        if (isNull(ex)) {
            response.setStatus(HttpServletResponse.SC_OK);
            response.setContentType("application/xml");
            response.setCharacterEncoding("UTF8");
        } else if (ex instanceof AppException) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.addHeader("APP_ERROR_MESSAGE", ex.getLocalizedMessage());
        } else {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    public static void main(String[] args) throws Exception {
        val server = new Server(8080);

        server.setHandler(new Servlet());
        server.start();
        server.join();
    }

}
