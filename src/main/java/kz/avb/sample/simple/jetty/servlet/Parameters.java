package kz.avb.sample.simple.jetty.servlet;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement
public class Parameters {

    public static class Parameter {
        private String name;
        private String value;

        public Parameter(String name, String value) {
            this.name = name;
            this.value = value;
        }

        @XmlAttribute
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @XmlValue
        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    private List<Parameter> numerics;

    private List<Parameter> strings;

    @XmlElement(name = "value")
    @XmlElementWrapper(name = "numeric_parameters")
    public List<Parameter> getNumerics() {
        return numerics;
    }

    public void setNumerics(List<Parameter> numerics) {
        this.numerics = numerics;
    }

    @XmlElement(name = "value")
    @XmlElementWrapper(name = "string_parameters")
    public List<Parameter> getStrings() {
        return strings;
    }

    public void setStrings(List<Parameter> strings) {
        this.strings = strings;
    }
}
